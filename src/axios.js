import Vue from 'vue'

import axios from 'axios'

let token = localStorage.token

let headers = {}
if (token) {
  headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  }
}

const axiosInstance = axios.create({
  baseURL: process.env.API_URL,
  headers: headers
})

Vue.prototype.$http = axiosInstance
