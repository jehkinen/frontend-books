import Vue from 'vue'

const constantsStore = new Vue({
  data: {
    ERROR_CODE_VALIDATION_FAILED: 422,
    ERROR_CODE_UNAUTHORIZED: 401
  }
})

Vue.prototype.$constants = constantsStore

export { constantsStore as default }
