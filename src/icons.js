import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit, faTrash, faBook, faPlusCircle, faBookOpen } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faEdit, faTrash, faBook, faPlusCircle, faBookOpen)
Vue.component('font-awesome-icon', FontAwesomeIcon)
