// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Multiselect from 'vue-multiselect'

import './constants'
import './routes'

import router from './router'

import './icons'
import './toastr'
import './axios'
Vue.config.productionTip = false

Vue.component('multiselect', Multiselect)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
