import Vue from 'vue'
import Router from 'vue-router'

import BookCreateUpdate from '@/components/book/CreateUpdate'
import BookList from '@/components/book/List'

import GenreCreateUpdate from '@/components/genre/CreateUpdate'
import GenreList from '@/components/genre/List'

import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/books',
      name: Vue.prototype.$routes.ROUTE_LIST_BOOK,
      component: BookList
    },
    {
      path: '/book/create',
      name: Vue.prototype.$routes.ROUTE_CREATE_BOOK,
      component: BookCreateUpdate
    },
    {
      path: '/book/update/:id',
      name: Vue.prototype.$routes.ROUTE_UPDATE_BOOK,
      component: BookCreateUpdate
    },
    {
      path: '/genres',
      name: Vue.prototype.$routes.ROUTE_LIST_GENRE,
      component: GenreList
    },
    {
      path: '/genre/create',
      name: Vue.prototype.$routes.ROUTE_CREATE_GENRE,
      component: GenreCreateUpdate
    },
    {
      path: '/genre/update/:id',
      name: Vue.prototype.$routes.ROUTE_UPDATE_GENRE,
      component: GenreCreateUpdate
    },
    {
      path: '/',
      name: Vue.prototype.$routes.ROUTE_LOGIN,
      component: Login
    }
  ]
})
