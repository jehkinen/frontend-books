import Vue from 'vue'

const routersStore = new Vue({
  data: {
    ROUTE_LIST_BOOK: 'book.list',
    ROUTE_UPDATE_BOOK: 'book.update',
    ROUTE_CREATE_BOOK: 'book.create',

    ROUTE_CREATE_GENRE: 'create.genre',
    ROUTE_UPDATE_GENRE: 'update.genre',
    ROUTE_LIST_GENRE: 'list.genre',
    ROUTE_LOGIN: 'login'
  }
})

Vue.prototype.$routes = routersStore

export { routersStore as default }
