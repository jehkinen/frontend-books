import Vue from 'vue'

import Toasted from 'vue-toasted'

const Options = {
  theme: 'primary',
  position: 'top-right',
  duration: 3000
}
Vue.use(Toasted, Options)
